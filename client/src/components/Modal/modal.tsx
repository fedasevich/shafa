import { cn } from '#/libs/helpers/cn';
import { useNestedModal } from '#/libs/hooks/useNestedModal';
import React, { ReactNode, useRef } from 'react';
import ReactDOM from 'react-dom';
import { CSSTransition } from 'react-transition-group';
import styles from './modalStyle.module.css';

interface ModalProps {
  active: boolean;
  setActive: (active: boolean) => void;
  children: ReactNode;
}

export function Modal({ active, setActive, children }: ModalProps) {
  const modalRef = useRef<HTMLDivElement>(null);

  const portalContainer = document.getElementById('modal-root');

  useNestedModal(modalRef, () => setActive(false));
  if (!portalContainer) return null;

  const nodeRef = React.useRef(null);

  return ReactDOM.createPortal(
    <CSSTransition
      nodeRef={nodeRef}
      in={active}
      timeout={2000}
      classNames={{
        enter: styles['custom_modal_content-enter'],
        enterActive: styles['custom_modal_content-enter-active'],
        enterDone: styles['custom_modal_content-enter-done'],
        exit: styles['custom_modal_content-exit'],
        exitActive: styles['custom_modal_content-exit-active'],
        exitDone: styles['custom_modal_content-exit-active']
      }}
    >
      <div className={cn(styles.custom_modal, active && styles.active)}>
        <div ref={nodeRef} className={cn(styles.custom_modal_content, active && styles.active)}>
          <div>{children}</div>
        </div>
      </div>
    </CSSTransition>,
    portalContainer
  );
}

export default Modal;
