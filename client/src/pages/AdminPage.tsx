import { ArrowDownOutlined, ArrowUpOutlined } from '@ant-design/icons';
import { Card, Col, Flex, Row, Space, Statistic, Timeline } from 'antd';
import { valueType } from 'antd/es/statistic/utils';
import CountUp from 'react-countup';

const formatter = (value: valueType) => <CountUp end={value as number} separator="," />;

export function AdminPage() {
  return (
    <>
      <Space direction="vertical" className="fle p-4">
        <Row gutter={[16, 16]} className="flex h-52   justify-center">
          <Col span={5}>
            <Flex justify="center" align="center" className="h-full rounded-lg bg-blue-500 text-center">
              <Statistic
                title={<h3 className="font-semibold text-white">Active Users</h3>}
                value={112893}
                valueStyle={{ color: 'white' }}
                formatter={formatter}
              />
            </Flex>
          </Col>
          <Col span={5}>
            <Flex justify="center" align="center" className="h-full rounded-lg bg-cyan-500 text-center text-white">
              <Statistic
                title={<h3 className="font-semibold text-white">Account Balance (CNY)</h3>}
                style={{ color: 'white' }}
                valueStyle={{ color: 'white' }}
                value={112893}
                precision={2}
                formatter={formatter}
              />
            </Flex>
          </Col>
          <Col span={5}>
            <Flex justify="center" align="center" className="h-full rounded-lg bg-pink-500 text-center text-white">
              <Statistic
                title={<h3 className="font-semibold text-white">New users</h3>}
                style={{ color: 'white' }}
                valueStyle={{ color: 'white' }}
                value={9798}
                precision={2}
                formatter={formatter}
              />
            </Flex>
          </Col>
          <Col span={5}>
            <Flex justify="center" align="center" className="h-full rounded-lg bg-green-500 text-center text-white">
              <Statistic
                title={<h3 className="font-semibold text-white">Unique visitors</h3>}
                style={{ color: 'white' }}
                valueStyle={{ color: 'white' }}
                value={15456}
                precision={2}
                formatter={formatter}
              />
            </Flex>
          </Col>
        </Row>
        <Row gutter={[16, 16]} className="mt-2 flex justify-center">
          <Col span={15}>
            <Card title="Sales chart" style={{ minHeight: '400px' }} />
          </Col>
          <Col span={5}>
            <Card className="flex  gap-10">
              <Card bordered={false} className="mb-2">
                <Statistic
                  title="Active"
                  value={11.28}
                  precision={2}
                  valueStyle={{ color: '#3f8600' }}
                  prefix={<ArrowUpOutlined />}
                  suffix="%"
                />
              </Card>
              <Card bordered={false} className="mb-2">
                <Statistic
                  title="Idle"
                  value={9.3}
                  precision={2}
                  valueStyle={{ color: '#cf1322' }}
                  prefix={<ArrowDownOutlined />}
                  suffix="%"
                />
              </Card>
              <Timeline
                className="mt-2"
                items={[
                  {
                    children: 'Create a services site 2015-09-01'
                  },
                  {
                    children: 'Solve initial network problems 2015-09-01'
                  },
                  {
                    children: 'Technical testing 2015-09-01'
                  },
                  {
                    children: 'Technical testing 2015-09-01'
                  }
                ]}
              />
            </Card>
          </Col>
        </Row>
      </Space>
    </>
  );
}
