/* eslint-disable react/no-unused-prop-types */
import { useAppDispatch } from '#/libs/hooks/redux';
import { addComment } from '#/store/reducers/ProductSlice';
import { EmptyStarIcon, StarIcon } from '#/ui/icons/icons';
import { notification } from 'antd';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import { useParams } from 'react-router-dom';
import { useProductCommentsFormValidation } from './validation';

export function ProductCommentsForm() {
  const dispatch = useAppDispatch();

  const { id } = useParams();

  const [api, contextHolder] = notification.useNotification();

  // const formik = useFormik({});

  return (
    <>
      {contextHolder}
      <Formik
        initialValues={{
          comment: '',
          rating: 0
        }}
        validationSchema={useProductCommentsFormValidation()}
        onSubmit={(values, { resetForm }) => {
          api.success({
            message: 'Success',
            description: 'Your comment has been successfully added!',
            placement: 'topRight'
          });

          dispatch(addComment({ id: Number(id), rating: values.rating, text: values.comment }));
          resetForm();
        }}
      >
        <Form>
          {/* <form onSubmit={formik.handleSubmit} className="mt-10 flex flex-col"> */}
          <div className="flex">
            <img
              src="https://shafa.c.prom.st/build/static/img/default-avatar.png"
              alt="avatar"
              className="relative bottom-4 h-10 w-10 pb-3 pr-2"
            />
            <div className="flex w-2/3 flex-col">
              <Field
                as="textarea"
                name="comment"
                placeholder="Текст повідомлення"
                className=" m-[2px] h-20 w-full rounded-md border-0 border-theme-green bg-gray-200 p-4 font-sans hover:m-0 hover:border-2 focus:m-0 focus:border-2 focus:border-theme-green focus:outline-0"
              />

              <ErrorMessage name="comment" component="p" className="text-red-500" />
            </div>
          </div>
          <div className="ml-12">
            <p className="mb-1 text-sm">Оберіть якість обслуговування:</p>
            <Field name="rating" id="rating" type="number">
              {({
                field: { value },
                form: { setFieldValue }
              }: {
                field: { value: number };
                form: { setFieldValue: (fieldName: string, value: number) => void };
              }) => (
                <div>
                  {Array.from({ length: 5 }, (_, index) => (
                    <button
                      type="button"
                      className="cursor-pointer border-0 bg-transparent p-0"
                      key={index}
                      onClick={() => setFieldValue('rating', index + 1)}
                      style={{ display: 'inline-block' }}
                    >
                      {index + 1 <= value ? <StarIcon /> : <EmptyStarIcon />}
                    </button>
                  ))}
                </div>
              )}
            </Field>
            <ErrorMessage name="rating" component="p" className="text-red-500" />
            {/* {formik.touched.rating && formik.errors.rating && (
                <div className="mt-1 text-sm text-red-500">{formik.errors.rating}</div>
              )} */}
            <div className=" mt-5">
              <button
                type="submit"
                className="cursor-pointer self-start rounded-md border-0 bg-theme-green px-10 py-3 text-center font-sans text-sm font-medium duration-150 hover:bg-opacity-70 hover:text-black"
              >
                Відправити
              </button>
            </div>
          </div>
          {/* </form> */}
        </Form>
      </Formik>
    </>
  );
}
