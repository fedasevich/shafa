import { USD_TO_UAH_EXCHANGE_RATE } from '#/libs/constants/exchangeRate';
import { useAppSelector } from '#/libs/hooks/redux';
import { useProductCardContext } from './ProductCardContext';

function ProductDiscount() {
  const {
    product: { oldPrice, discountPercent }
  } = useProductCardContext();
  const {
    user: { userCurrency }
  } = useAppSelector((state) => state.userReducer);

  if ((!oldPrice && !discountPercent) || !oldPrice) {
    return null;
  }

  return (
    <div className="flex ">
      <p className="my-0 text-sm text-gray-400 line-through">
        {userCurrency === '₴' ? oldPrice : (oldPrice / USD_TO_UAH_EXCHANGE_RATE).toFixed(2)} {userCurrency}
      </p>
      <p className="my-0 ml-2 rounded-md border border-solid border-pink-400 px-[1px] text-sm text-pink-400">
        -{discountPercent}%
      </p>
    </div>
  );
}

export default ProductDiscount;
