interface ProductTagContainerProps {
  children: React.ReactNode;
}

function ProductTagContainer({ children }: ProductTagContainerProps) {
  return (
    <>
      <div className="absolute left-4 top-4 flex">{children}</div>
    </>
  );
}

export default ProductTagContainer;
