import AppLink from '#/components/AppLink/AppLink';

function FooterAddress() {
  return (
    <div>
      <AppLink
        target="_blank"
        href="https://shafa.kayako.com/category/1-kak-eto-rabotaet"
        rel="noreferrer"
        title=" Як це працює?"
      />
      <p className="mt-6 text-sm font-normal">
        Україна, 02121, місто Київ, Харківське шосе, будинок 201-203, літера 4Г
      </p>
    </div>
  );
}

export default FooterAddress;
