import { SHOP_ROUTE_CATEGORY } from '#/libs/constants/routes';
import { useDebounce } from '#/libs/hooks/useDebounce';
import { Input, Select, Space } from 'antd';
import { ChangeEvent, useEffect, useState } from 'react';
import { generatePath, useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import { cn } from '../../../libs/helpers/cn';
import { LeftArrowIcon, SearchIcon } from '../../../ui/icons/icons';

const options = [
  { title: 'Всі категорії', value: '' },
  { title: 'Жінкам', value: 'forWomen' },
  { title: 'Чоловікам', value: 'forMen' },
  { title: 'Для дому', value: 'forHome' },
  { title: "Краса і здоров'я", value: 'beautyAndHealth' },
  { title: 'Дитячі товари', value: 'kidsProducts' },
  { title: 'Спорт і відпочинок', value: 'sportsAndRecreation' }
];

function SearchButton() {
  return (
    <div className="h-6 w-6 lg:h-5 lg:w-5">
      <SearchIcon />
    </div>
  );
}

interface NavbarSearchProps {
  isOpen: boolean;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const selectOptions = options.map(({ title, value }) => ({
  value: value.toLowerCase(),
  label: title
}));

interface SearchBarProps {
  isOpen?: boolean;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  value: string;
}

function SearchBar({ isOpen, onChange, value }: SearchBarProps) {
  const location = useLocation();
  const defaultSelectValue = location.pathname.split('/')[2] || options[0].title;
  const navigate = useNavigate();

  const handleCategoryChange = (value: string) => {
    navigate(generatePath(SHOP_ROUTE_CATEGORY, { category: value }), { replace: true });
  };

  return (
    <Space.Compact size="large" className={cn(isOpen && 'w-full')}>
      <Select
        onSelect={handleCategoryChange}
        defaultValue={defaultSelectValue || options[0].title}
        options={selectOptions}
      />
      <Input placeholder="input search text" suffix={<SearchButton />} onChange={onChange} value={value} />
    </Space.Compact>
  );
}

export function NavbarSearch({ setIsOpen, isOpen }: NavbarSearchProps) {
  const [searchParams, setSearchParams] = useSearchParams({ query: '' });

  const [searchQuery, setSearchQuery] = useState<string>(searchParams.get('query') || '');
  const debouncedValue = useDebounce<string>(searchQuery, 500);

  useEffect(() => {
    setSearchParams((prev) => {
      prev.set('query', debouncedValue);
      return prev;
    });
  }, [debouncedValue]);

  const handleSearchChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(event.target.value);
  };

  const handleResponsiveSearchClick = () => {
    setIsOpen(true);
  };

  return (
    <>
      <div className={cn('lg:w-96', isOpen && 'w-full')}>
        <div className="hidden lg:inline-flex">
          <SearchBar onChange={handleSearchChange} value={searchQuery} />
        </div>

        {!isOpen && (
          // eslint-disable-next-line jsx-a11y/control-has-associated-label
          <button className="border-0 bg-transparent lg:hidden" onClick={handleResponsiveSearchClick} type="button">
            <SearchButton />
          </button>
        )}
        <div className="block w-full lg:hidden">
          {isOpen && (
            <div className="flex w-full">
              {/* eslint-disable-next-line jsx-a11y/control-has-associated-label */}
              <button
                type="button"
                className="border-0 bg-transparent pt-1"
                onClick={() => {
                  setIsOpen(false);
                }}
              >
                <LeftArrowIcon />
              </button>
              <SearchBar isOpen onChange={handleSearchChange} value={searchQuery} />
            </div>
          )}
        </div>
      </div>
    </>
  );
}
