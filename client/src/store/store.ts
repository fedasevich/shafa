import { combineReducers, configureStore } from '@reduxjs/toolkit';

import cartReducer from './reducers/CartSlice';
import productReducer from './reducers/ProductSlice';
import userReducer from './reducers/UserSlice';

export const rootReducer = combineReducers({
  // teacherReducer,
  // productReducer,
  cartReducer,
  userReducer,
  productReducer
  // checkOutReducer,
  // [groupApi.reducerPath]: groupApi.reducer,
  // [shopApi.reducerPath]: shopApi.reducer,
  // [orderApi.reducerPath]: orderApi.reducer,
});

export const setupStore = () => {
  return configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware(),
    // .concat(groupApi.middleware)
    // .concat(shopApi.middleware)
    // .concat(orderApi.middleware),,
    devTools: true
  });
};

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore['dispatch'];
