import { number, object, string } from 'yup';

export const useProductCommentsFormValidation = () => {
  return object({
    comment: string().required('Будь ласка, надайте коментар перед відправленням.'),
    rating: number().min(1, 'Будь ласка, виберіть рейтинг перед відправленням.').required()
  });
};
