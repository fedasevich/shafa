import { ReactNode } from 'react';

interface FooterRowProps {
  children: ReactNode;
}

function FooterRow({ children }: FooterRowProps) {
  return (
    <div className="mx-auto grid w-10/12 grid-cols-1 gap-6 text-center md:grid-cols-1 lg:grid-cols-5 lg:text-start 2xl:w-2/3">
      {children}
    </div>
  );
}

export default FooterRow;
