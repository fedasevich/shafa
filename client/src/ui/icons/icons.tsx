import { ReactComponent as Burger } from '#assets/icons/burger.svg';
import { ReactComponent as Cart } from '#assets/icons/cart.svg';
import { ReactComponent as Copy } from '#assets/icons/copy.svg';
import { ReactComponent as EmptyStar } from '#assets/icons/empty-star.svg';
import { ReactComponent as Facebook } from '#assets/icons/facebook.svg';
import { ReactComponent as Heart } from '#assets/icons/heart.svg';
import { ReactComponent as ClickedHeart } from '#assets/icons/heart_clicked.svg';
import { ReactComponent as Instagram } from '#assets/icons/instagram.svg';
import { ReactComponent as LeftArrow } from '#assets/icons/left-arrow.svg';
import { ReactComponent as Logo } from '#assets/icons/logo.svg';
import { ReactComponent as Search } from '#assets/icons/search.svg';
import { ReactComponent as Star } from '#assets/icons/star.svg';
import { ReactComponent as Truck } from '#assets/icons/truck.svg';
import { ReactComponent as Ukraine } from '#assets/icons/ukraine.svg';
import { ReactComponent as Youtube } from '#assets/icons/youtube.svg';

export function LogoIcon() {
  return <Logo />;
}

export function SearchIcon() {
  return <Search />;
}

export function CartIcon() {
  return <Cart />;
}

export function LeftArrowIcon() {
  return <LeftArrow />;
}

export function BurgerIcon() {
  return <Burger />;
}

export function FacebookIcon() {
  return <Facebook />;
}
export function InstagramIcon() {
  return <Instagram />;
}

export function YoutubeIcon() {
  return <Youtube />;
}

export function HeartIcon() {
  return <Heart />;
}
export function ClickedHeartIcon() {
  return <ClickedHeart />;
}

export function UkraineIcon() {
  return <Ukraine />;
}

export function TruckIcon() {
  return <Truck />;
}

export function StarIcon() {
  return <Star />;
}

export function EmptyStarIcon() {
  return <EmptyStar />;
}

export function CopyIcon() {
  return <Copy />;
}
