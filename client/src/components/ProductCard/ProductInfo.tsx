import { ReactNode } from 'react';
import ProductCard from './ProductCard';

export type Props = {
  children: ReactNode;
};

function ProductInfo({ children }: Props) {
  return (
    <div className="flex justify-between">
      <div className="">{children}</div>
      <ProductCard.Like />
    </div>
  );
}

export default ProductInfo;
