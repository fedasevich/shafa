import { Button, Form, Input, Modal, notification } from 'antd';
import { useFormik } from 'formik';
import { useState } from 'react';
import { useAuthFormValidation } from './validation';

interface AuthModalProps {
  visible: boolean;
  setVisible: React.Dispatch<React.SetStateAction<boolean>>;
}

function AuthModal({ visible, setVisible }: AuthModalProps) {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [api, contextHolder] = notification.useNotification();
  const formik = useFormik({
    initialValues: {
      firstname: '',
      lastName: '',
      password: ''
    },
    validationSchema: useAuthFormValidation(),
    onSubmit: () => {
      setIsLoading(true);
      api.success({
        message: 'Success',
        description: 'Successfully signed up!',
        placement: 'topRight'
      });
    }
  });

  const onCancel = () => {
    setVisible(false);
  };

  return (
    <>
      {contextHolder}
      <Modal title="Sign Up" open={visible} onCancel={onCancel} footer={[]}>
        <Form layout="vertical" onFinish={formik.handleSubmit}>
          <Form.Item
            label="First Name"
            validateStatus={formik.errors.firstname && formik.touched.firstname ? 'error' : ''}
            help={formik.errors.firstname && formik.touched.firstname ? formik.errors.firstname : ''}
          >
            <Input
              name="firstname"
              value={formik.values.firstname}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </Form.Item>
          <Form.Item
            label="Last Name"
            validateStatus={formik.errors.lastName && formik.touched.lastName ? 'error' : ''}
            help={formik.errors.lastName && formik.touched.lastName ? formik.errors.lastName : ''}
          >
            <Input
              name="lastName"
              value={formik.values.lastName}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </Form.Item>
          <Form.Item
            label="Password"
            validateStatus={formik.errors.password && formik.touched.password ? 'error' : ''}
            help={formik.errors.password && formik.touched.password ? formik.errors.password : ''}
          >
            <Input.Password
              name="password"
              value={formik.values.password}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </Form.Item>
          <div className="flex justify-end gap-4">
            <Button key="back" onClick={onCancel}>
              Cancel
            </Button>
            <Button
              loading={isLoading}
              key="submit"
              htmlType="submit"
              type="primary"
              onClick={() => formik.handleSubmit}
            >
              Sign Up
            </Button>
          </div>
        </Form>
      </Modal>
    </>
  );
}

export default AuthModal;
