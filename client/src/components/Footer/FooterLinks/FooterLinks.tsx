import AppLink from '#/components/AppLink/AppLink';

function FooterLinks() {
  return (
    <div>
      <ul className="list-none pl-0">
        <li className="mt-2">
          <AppLink href="/privacy-policy" title="Політика конфіденційності" className="text-sm" />
        </li>
        <li className="mt-2">
          <AppLink href="/terms-of-service" title="Договір-оферта" className="text-sm" />
        </li>
        <li className="mt-2">
          <AppLink href="/contacts" title="Контакти" className="text-sm" />
        </li>
      </ul>
    </div>
  );
}

export default FooterLinks;
