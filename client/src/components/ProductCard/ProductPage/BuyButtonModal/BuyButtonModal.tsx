import Modal from '#/components/Modal/modal';
import React from 'react';

import styles from './buyButton.module.css';

interface BuyModalProps {
  buyModalModalActive: boolean;
  setBuyModalActive: React.Dispatch<React.SetStateAction<boolean>>;
}

export function BuyButtonModal({ buyModalModalActive, setBuyModalActive }: BuyModalProps) {
  const handleButtonClick = () => {
    setBuyModalActive(false);
  };
  return (
    <div className="">
      <Modal active={buyModalModalActive} setActive={setBuyModalActive}>
        <div className={styles.wrapper}>
          <div className={styles.header}>
            <h3>Швидка покупка</h3>
          </div>
          <div className={styles.body}>
            <p>Ваше замовлення було додано до кошика. Перевірте його перед оформленням.</p>
          </div>
          <div className={styles.footer}>
            <button onClick={handleButtonClick} className={styles.footerButton} type="button">
              Закрити
            </button>
          </div>
        </div>
      </Modal>
    </div>
  );
}
