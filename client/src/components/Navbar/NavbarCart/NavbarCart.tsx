import { ClassValue } from 'clsx';
import { useMemo } from 'react';
import { cn } from '../../../libs/helpers/cn';
import { useAppSelector } from '../../../libs/hooks/redux';
import { CartIcon } from '../../../ui/icons/icons';

interface NavbarCartProps {
  className: ClassValue;
}

export function NavbarCart({ className }: NavbarCartProps) {
  const { products } = useAppSelector((state) => state.productReducer);

  const likedProductsCount = useMemo(() => {
    return products.filter(({ node }) => node.isLiked, 0).length;
  }, [products]);

  return (
    <div className={cn('relative h-6 w-6', className)}>
      <CartIcon />
      <div
        className="absolute right-0 top-0 -mr-1 -mt-1 flex h-4 w-4 items-center justify-center rounded-full bg-theme-green text-xs
       text-white"
      >
        {likedProductsCount}
      </div>
    </div>
  );
}
