import useHistoricNavigate from '#/libs/hooks/useHistoricNavigate';
import { ClassValue } from 'clsx';
import { Link } from 'react-router-dom';
import { cn } from '../../libs/helpers/cn';

interface AppLinkProps extends Omit<React.HTMLProps<HTMLAnchorElement>, 'className'> {
  title: string;
  href: string;
  className?: ClassValue;
}

function AppLink({ title, href, className }: AppLinkProps) {
  const { onLinkClick } = useHistoricNavigate();

  const handleLinkClick = () => {
    onLinkClick(href);
  };

  return (
    <Link
      to={href}
      onClick={handleLinkClick}
      className={cn(
        'font-sans text-black decoration-transparent duration-150 hover:text-theme-green hover:underline hover:decoration-theme-green',
        className
      )}
    >
      {title}
    </Link>
  );
}

export default AppLink;
