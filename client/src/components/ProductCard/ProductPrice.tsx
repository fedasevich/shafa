import { USD_TO_UAH_EXCHANGE_RATE } from '#/libs/constants/exchangeRate';
import { cn } from '#/libs/helpers/cn';
import { useAppSelector } from '#/libs/hooks/redux';
import { ClassNamed } from '#/libs/types/ClassNamed/ClassNamed.type';
import { useProductCardContext } from './ProductCardContext';

function ProductPrice({ className }: ClassNamed) {
  const { product } = useProductCardContext();
  const {
    user: { userCurrency }
  } = useAppSelector((state) => state.userReducer);

  return (
    <p className={cn('mb-0 mt-1 text-lg font-medium', className)}>
      {userCurrency === '₴' ? product.price : (product.price / USD_TO_UAH_EXCHANGE_RATE).toFixed(2)} {userCurrency}
    </p>
  );
}

export default ProductPrice;
