import AppLink from '#/components/AppLink/AppLink';

function FooterRights() {
  return (
    <div className="col-span-2">
      <p>Речі за кліком серця. Всі права захищені</p>
      <p>
        © 2023&nbsp;
        <AppLink href="/uk/" title="Shafa.ua" />
      </p>
    </div>
  );
}

export default FooterRights;
