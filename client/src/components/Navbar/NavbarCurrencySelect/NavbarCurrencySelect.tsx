import { useAppDispatch, useAppSelector } from '#/libs/hooks/redux';
import { UserCurrency } from '#/libs/types/User/UserCurrency.type';
import { setUserCurrency } from '#/store/reducers/UserSlice';
import { Typography } from 'antd';
import { ClassValue } from 'clsx';
import { cn } from '../../../libs/helpers/cn';

const languages: UserCurrency[] = ['₴', '$'];
const { Text } = Typography;

interface NavbarCurrencySelectProps {
  className: ClassValue;
}

export function NavbarCurrencySelect({ className }: NavbarCurrencySelectProps) {
  const {
    user: { userCurrency }
  } = useAppSelector((state) => state.userReducer);
  const dispatch = useAppDispatch();

  const handleCurrencySelect = (currency: UserCurrency) => {
    dispatch(setUserCurrency(currency));
  };

  return (
    <Text className={cn(className)}>
      {languages.map((currency, index) => (
        <span key={currency}>
          <Text
            className="text-sm"
            strong={userCurrency === currency}
            onClick={() => handleCurrencySelect(currency)}
            style={{ cursor: 'pointer' }}
          >
            {currency}
          </Text>
          {index < languages.length - 1 && <Text className="text-sm"> | </Text>}
        </span>
      ))}
    </Text>
  );
}
