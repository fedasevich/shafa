import { RefObject } from 'react';
import { useOnClickOutside } from './useOnClickOutside';

type Handler = (event: MouseEvent) => void;

export function useNestedModal<T extends HTMLElement = HTMLElement>(nestedRef: RefObject<T>, handler: Handler) {
  const nestedHandler = (event: Event) => {
    const modalRoot = document.getElementById('modal-root');
    const activeModals = modalRoot?.querySelectorAll('.custom_modal.active');

    const isNestedModalClick = Array.from(activeModals || []).some(
      (modal) => modal.contains(nestedRef.current) && modal !== event.target
    );

    if (isNestedModalClick) {
      return;
    }

    handler(event as MouseEvent);
  };

  useOnClickOutside(nestedRef, nestedHandler);
}
