import { Product } from '#/libs/types/Products/Product.type';
import { ProductCommentsForm } from './ProductCommentsForm';
import { ProductCommentsItem } from './ProductCommentsItem';

interface ProductCommentsProps {
  product: Product;
}

export function ProductComments({ product }: ProductCommentsProps) {
  const { comments } = product;

  return (
    <div className="">
      <h2 className="mt-0">Залишити відгук</h2>
      <ProductCommentsForm />
      <div className="mt-8">
        <h2 className="mt-0">Відгуки про товар</h2>
        {comments.map((comment) => (
          <ProductCommentsItem key={comment.id} comment={comment} />
        ))}
      </div>
    </div>
  );
}
