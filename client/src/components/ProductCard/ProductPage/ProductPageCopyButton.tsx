import { cn } from '#/libs/helpers/cn';
import { useCopyToClipboard } from '#/libs/hooks/useCopyToClipboard';
import { ClassNamed } from '#/libs/types/ClassNamed/ClassNamed.type';
import { CopyIcon } from '#/ui/icons/icons';
import { notification } from 'antd';

function ProductPageCopyButton({ className }: ClassNamed) {
  const [api, contextHolder] = notification.useNotification();

  const copy = useCopyToClipboard({ api });
  const currentFullLocation = window.location.href;

  const handleButtonClick = async () => {
    await copy(currentFullLocation);
  };

  return (
    <>
      {contextHolder}

      <button
        type="button"
        className={cn('flex cursor-pointer items-center border-0 bg-transparent', className)}
        onClick={handleButtonClick}
      >
        <p className="pe-2 text-sm font-medium">Поділитися:</p>
        <div className="text-xl">
          <CopyIcon />
        </div>
      </button>
    </>
  );
}

export default ProductPageCopyButton;
