import { object, string } from 'yup';

export const useAuthFormValidation = () => {
  return object().shape({
    firstname: string().min(3, 'Ім’я має бути не менше 3 символів').required('Це поле обов’язкове'),
    lastName: string().max(40, 'Прізвище не більше 40 символів').required('Це поле обов’язкове'),
    password: string()
      .matches(/[A-Z]/, 'В паролі має бути хоча б одна велика літера')
      .matches(/[^a-zA-Z]/, 'В паролі має бути хоча б один знак відмінний від букви')
      .required('Це поле обов’язкове')
  });
};
