import { ReactNode, useMemo } from 'react';
import { CSSTransition } from 'react-transition-group';
import { cn } from '../../libs/helpers/cn';
import { ClassNamed } from '../../libs/types/ClassNamed/ClassNamed.type';
import { Product } from '../../libs/types/Products/Product.type';
import ProductBrand from './ProductBrand';
import ProductCardContext from './ProductCardContext';
import styles from './ProductCardStyle.module.css';
import ProductDiscount from './ProductDiscount';
import ProductImage from './ProductImage';
import ProductInfo from './ProductInfo';
import ProductLike from './ProductLike';
import ProductName from './ProductName';
import ProductCardPage from './ProductPage/ProductCardPage';
import ProductPageName from './ProductPage/ProductPageName';
import ProductPrice from './ProductPrice';
import ProductRating from './ProductRating';
import ProductSize from './ProductSize';
import ProductTag from './ProductTag/ProductTag';
import ProductTagContainer from './ProductTag/ProductTagContainer';

type Props = {
  product: Product;
  image?: ReactNode;
  info?: ReactNode;
} & ClassNamed;

function ProductCard({ image, info, product, className }: Props) {
  const memoizedProduct = useMemo(() => ({ product }), [product]);

  return (
    <ProductCardContext.Provider value={memoizedProduct}>
      <CSSTransition
        timeout={400}
        in
        classNames={{
          enter: styles['product-card-enter'],
          enterActive: styles['product-card-enter-active']
          // enter:styles[],
        }}
        appear
      >
        <div className={cn('block', className)}>
          {image}
          <div>{info}</div>
        </div>
      </CSSTransition>
    </ProductCardContext.Provider>
  );
}

ProductCard.Image = ProductImage;
ProductCard.Name = ProductName;
ProductCard.Info = ProductInfo;
ProductCard.Rating = ProductRating;
ProductCard.Price = ProductPrice;
ProductCard.Discount = ProductDiscount;
ProductCard.Like = ProductLike;
ProductCard.Size = ProductSize;
ProductCard.Brand = ProductBrand;
ProductCard.TagContainer = ProductTagContainer;
ProductCard.Tag = ProductTag;
ProductCard.ProductPage = ProductCardPage;
ProductCard.ProductPage.Name = ProductPageName;

export default ProductCard;
