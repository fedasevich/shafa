import { ReactNode } from 'react';
import ProductPageButtons from './ProductCardButtons';
import ProductPageName from './ProductPageName';
import ProductPageSize from './ProductPageSize';

interface ProductCardPageProps {
  children: ReactNode;
}

export function ProductCardPage({ children }: ProductCardPageProps) {
  return <>{children}</>;
}

ProductCardPage.Name = ProductPageName;
ProductCardPage.Size = ProductPageSize;
ProductCardPage.Buttons = ProductPageButtons;

export default ProductCardPage;
