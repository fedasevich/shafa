import { UkraineIcon } from '#/ui/icons/icons';
import ProductTag from './ProductTag';

function UkrainianTag() {
  return (
    <>
      <ProductTag icon={<UkraineIcon />} tag="UKRBRAND" />
    </>
  );
}

export default UkrainianTag;
