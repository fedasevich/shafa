import { cn } from '#/libs/helpers/cn';
import { ClassNamed } from '#/libs/types/ClassNamed/ClassNamed.type';
import { Button } from 'antd';
import { useState } from 'react';
import { BuyButtonModal } from './BuyButtonModal/BuyButtonModal';

function ProductPageButtons({ className }: ClassNamed) {
  const [buyModalActive, setBuyModalActive] = useState<boolean>(false);

  const handleBuyButtonClick = () => {
    console.log('buy');
    setBuyModalActive(true);
  };

  const handleCartButtonClick = () => {
    console.log('cart');
  };

  return (
    <div className={cn('mt-6', className)}>
      <div className="flex flex-col gap-2 md:flex-row">
        <button
          type="button"
          onClick={handleBuyButtonClick}
          className={cn(
            'w-full cursor-pointer self-start rounded-md border-0 bg-theme-green px-16 py-3 text-center font-sans text-sm font-medium duration-150 hover:bg-opacity-70 hover:text-black'
          )}
        >
          Купити зараз
        </button>
        <Button
          onClick={handleCartButtonClick}
          type="text"
          className="h-[44px] w-full cursor-pointer self-start rounded-md border border-black px-16 py-3 text-center font-sans text-sm font-medium duration-150"
        >
          Додати до кошика
        </Button>
      </div>

      <BuyButtonModal buyModalModalActive={buyModalActive} setBuyModalActive={setBuyModalActive} />
    </div>
  );
}

export default ProductPageButtons;
