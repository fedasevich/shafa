import { cn } from '#/libs/helpers/cn';
import { ClassNamed } from '#/libs/types/ClassNamed/ClassNamed.type';
import { Size } from '#/libs/types/Products/Size.type';
import { Button } from 'antd';
import { useState } from 'react';
import { useProductCardContext } from '../ProductCardContext';

function ProductPageSize({ className }: ClassNamed) {
  const {
    product: { sizes }
  } = useProductCardContext();

  const [selectedSize, setSelectedSize] = useState<Size>(sizes[0]);

  const handleButtonClick = (size: Size) => {
    setSelectedSize(size);
  };

  return (
    <div className={cn(className)}>
      <p className="mb-2 text-gray-400">Розміри:</p>
      <div className="flex gap-2">
        {sizes.map((size) => (
          <Button
            onClick={() => handleButtonClick(size)}
            key={size.id}
            className={cn(
              'border-1 h-10 self-start border-black bg-transparent py-0 text-center text-xs lg:self-center',
              selectedSize.id === size.id && 'border-theme-green text-theme-green'
            )}
          >
            {size.name}
          </Button>
        ))}
      </div>
    </div>
  );
}

export default ProductPageSize;
