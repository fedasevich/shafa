import { cn } from '#/libs/helpers/cn';
import { ClassNamed } from '#/libs/types/ClassNamed/ClassNamed.type';
import { useProductCardContext } from '../ProductCardContext';

function ProductPageName({ className }: ClassNamed) {
  const {
    product: { name }
  } = useProductCardContext();
  return <h1 className={cn('text-2xl font-medium', className)}>{name}</h1>;
}

export default ProductPageName;
