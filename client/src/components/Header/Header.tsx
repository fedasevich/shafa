import { Navbar } from '../Navbar/Navbar';
import NavbarAlert from '../Navbar/NavbarAlert/NavbarAlert';

interface HeaderProps {
  isShowAlert: boolean;
}

export function Header({ isShowAlert }: HeaderProps) {
  return (
    <div className="flex flex-col-reverse bg-gray-200 lg:flex-col">
      {isShowAlert && <NavbarAlert />}
      <Navbar />
    </div>
  );
}
