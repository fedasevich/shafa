import { FacebookIcon, InstagramIcon, YoutubeIcon } from '#/ui/icons/icons';

function FooterSocials() {
  return (
    <div>
      <p className="text-sm">Ми у соц.мережах</p>
      <div className="flex justify-center gap-3 lg:justify-start">
        <div className="h-6 w-6">
          <FacebookIcon />
        </div>
        <InstagramIcon />
        <YoutubeIcon />
      </div>
    </div>
  );
}

export default FooterSocials;
