import FooterAddress from './FooterAddress/FooterAddress';
import FooterAppAdvert from './FooterAppAdvert/FooterAppAdvert';
import FooterLinks from './FooterLinks/FooterLinks';
import FooterRights from './FooterRights/FooterRights';
import FooterRow from './FooterRow';
import FooterSocials from './FooterSocials/FooterSocials';

function Footer() {
  return (
    <footer className="w-full bg-gray-200 py-10 font-sans">
      <FooterRow>
        <FooterAppAdvert />
        <FooterAddress />
        <FooterLinks />
        <FooterSocials />
      </FooterRow>
      <FooterRow>
        <FooterRights />
      </FooterRow>
    </footer>
  );
}

export default Footer;
