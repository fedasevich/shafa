import { FeedEdge } from '../libs/types/Products/FeedEdge.type';

export const productsMock: FeedEdge[] = [
  {
    node: {
      id: 133224572,
      url: '/uk/kids/dlya-malyshey/kostiumy-i-komplekty/133224572-dityachiy-nabir-sorochka-i-zhiletka-z-galstukom',
      thumbnail: 'https://image-thumbs.shafastatic.net/1332950473_310_430',
      name: 'Дитячий набір сорочка і жилетка з галстуком.',
      price: 250,
      oldPrice: 300,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: 17,
      likes: 11,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 46326,
        name: 'Bluezoo',
        __typename: 'Brand'
      },
      catalogSlug: 'kidsProducts',
      isNew: true,
      sizes: [
        {
          id: 126,
          name: '9-12мес',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: true,
      ownerHasRecentActivity: true,
      tags: ['FREE_SHIPPING', 'UKRBRAND'],
      rating: null,
      ratingAmount: null,
      comments: [
        { from: 'Svitlana', id: 1, text: 'Дякую,все сподобалось.', date: '11.10.2023', rating: 5 },
        {
          from: 'Наталія',
          id: 18,
          text: 'Товар прийшов у відмінному стані і швидко. Рекомендую цей магазин!',
          date: '11.10.2023',
          rating: 5
        },
        {
          from: 'Тарас',
          id: 19,
          text: 'Не розумію, чому ціна така висока. Потрібно більше знижок.',
          date: '11.10.2023',
          rating: 2
        },
        {
          from: 'Світлана',
          id: 20,
          text: 'Я задоволений цією покупкою, але розмір трохи відрізняється від заявленого.',
          date: '11.10.2023',
          rating: 4
        },
        {
          from: 'Ігор',
          id: 21,
          text: 'Замовив товар і вже отримав сьогодні. Дуже задоволений швидкістю доставки!',
          date: '11.10.2023',
          rating: 5
        },
        {
          from: 'Оксана',
          id: 22,
          text: 'Товар відмінний, але ціна трохи висока. Дякую за знижку!',
          date: '11.10.2023',
          rating: 4
        },
        {
          from: 'Володимир',
          id: 23,
          text: 'Бренд цього товару мене приємно вразив. Якість на висоті.',
          date: '11.10.2023',
          rating: 5
        },
        {
          from: 'Тетяна',
          id: 24,
          text: 'Товар прийшов в недоброму стані через погану упаковку. Рекомендую уважність.',
          date: '11.10.2023',
          rating: 2
        },
        {
          from: 'Андрій',
          id: 25,
          text: 'Хороший товар за розумною ціною. Рекомендую всім!',
          date: '11.10.2023',
          rating: 4
        }
      ]
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 118565134,
      url: '/uk/kids/dlya-devochek/futbolki/118565134-hit-sezona-futbolka-vensdyay',
      thumbnail: 'https://image-thumbs.shafastatic.net/921528551_310_430',
      name: 'Хіт сезону футболка  венсдей',
      price: 395,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 8,
      isLiked: false,
      __typename: 'Product',
      brand: null,
      catalogSlug: 'kidsProducts',
      isNew: true,
      sizes: [
        {
          id: 71,
          name: '12 / 152',
          __typename: 'Size'
        },
        {
          id: 72,
          name: '13 / 158',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: null,
      ratingAmount: null,
      comments: [
        { from: 'Михайло', id: 13, text: 'Цей товар - справжній знахідка! Рекомендую!', date: '11.10.2023', rating: 5 },
        {
          from: 'Вікторія',
          id: 14,
          text: 'Бажав би більше фотографій товару з різних ракурсів.',
          date: '11.10.2023',
          rating: 3
        },
        {
          from: 'Олена',
          id: 15,
          text: 'Якість підніжок на рівні, але розмір трохи не підійшов.',
          date: '11.10.2023',
          rating: 3
        },
        {
          from: 'Віталій',
          id: 16,
          text: 'Замовив цей товар для друга в подарунок. Він захоплений!',
          date: '11.10.2023',
          rating: 5
        },
        {
          from: 'Ігор',
          id: 17,
          text: 'Інформація про композицію тканини була б корисною.',
          date: '11.10.2023',
          rating: 4
        }
      ]
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 133227184,
      url: '/uk/women/shtany/dzhinsy/133227184-dzhinsi-stretch',
      thumbnail: 'https://image-thumbs.shafastatic.net/1333004625_310_430',
      name: 'Джинси стретч',
      price: 250,
      oldPrice: 300,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: 17,
      likes: 9,
      isLiked: false,
      __typename: 'Product',
      brand: null,
      catalogSlug: 'Жінкам',
      isNew: false,
      sizes: [
        {
          id: 52,
          name: '29 / M',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: ['FREE_SHIPPING'],
      rating: null,
      ratingAmount: null,
      comments: [
        {
          from: 'Марія',
          id: 6,
          text: 'Замовив цей товар і зараз чекаю, щоб спробувати. Сподіваюся, він підійде!',
          date: '11.10.2023',
          rating: 5
        },
        { from: 'Андрій', id: 7, text: 'Дуже швидка доставка і чудовий товар! Дякую!', date: '11.10.2023', rating: 5 },
        {
          from: 'Ірина',
          id: 8,
          text: 'Знижка на цей товар дуже доречна. Раджу купити!',
          date: '11.10.2023',
          rating: 4
        },
        {
          from: 'Олександр',
          id: 9,
          text: 'Це вже моя друга покупка цього товару. Дуже задоволений!',
          date: '11.10.2023',
          rating: 5
        }
      ]
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 133232243,
      url: '/uk/men/rubashki/rubashki/133232243-sorochka-cholovicha-prada',
      thumbnail: 'https://image-thumbs.shafastatic.net/1333076908_310_430',
      name: 'Сорочка чоловіча prada',
      price: 499,
      oldPrice: 600,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: 17,
      likes: 7,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 416,
        name: 'Prada',
        __typename: 'Brand'
      },
      catalogSlug: 'forMen',
      isNew: false,
      sizes: [
        {
          id: 150,
          name: '40 / L / 48',
          __typename: 'Size'
        },
        {
          id: 151,
          name: '42 / XL / 50',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: ['FREE_SHIPPING'],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 133231470,
      url: '/uk/women/verhnyaya-odezhda/kurtki/133231470-kurtka-zhinocha-demisezonna',
      thumbnail: 'https://image-thumbs.shafastatic.net/1333067767_310_430',
      name: 'Куртка жіноча демісезонна',
      price: 350,
      oldPrice: 400,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: 13,
      likes: 21,
      isLiked: false,
      __typename: 'Product',
      brand: null,
      catalogSlug: 'forWomen',
      isNew: false,
      sizes: [
        {
          id: 4,
          name: '38 / M / 46',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: ['FREE_SHIPPING'],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 133028883,
      url: '/uk/women/zhenskaya-obuv/baletki/133028883-baletki-z-hutrom',
      thumbnail: 'https://image-thumbs.shafastatic.net/1328782060_310_430',
      name: 'Балетки з хутром',
      price: 399,
      oldPrice: 500,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: 21,
      likes: 89,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 18142,
        name: 'M&S Collection',
        __typename: 'Brand'
      },
      catalogSlug: 'forWomen',
      isNew: false,
      sizes: [
        {
          id: 36,
          name: '39',
          __typename: 'Size'
        },
        {
          id: 231,
          name: '39.5',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: ['FREE_SHIPPING'],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 101614479,
      url: '/uk/men/aksessuary/drugie-aksessuary/101614479-zont-tri-slona',
      thumbnail: 'https://image-thumbs.shafastatic.net/1095320037_310_430',
      name: 'Парасоля три слона',
      price: 563,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 92,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 44559,
        name: 'Elefant',
        __typename: 'Brand'
      },
      catalogSlug: 'forHome',
      isNew: true,
      sizes: [
        {
          id: 11,
          name: 'One size',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: 5,
      ratingAmount: 1,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 132897507,
      url: '/uk/kids/dlya-malchikov/dzhinsy/132897507-siri-dzhinsi',
      thumbnail: 'https://image-thumbs.shafastatic.net/1325407749_310_430',
      name: 'Сірі джинси',
      price: 220,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 34,
      isLiked: false,
      __typename: 'Product',
      brand: null,
      catalogSlug: 'forWomen',
      isNew: false,
      sizes: [
        {
          id: 70,
          name: '11 / 146',
          __typename: 'Size'
        },
        {
          id: 71,
          name: '12 / 152',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 133050058,
      url: '/uk/kids/dlya-devochek/kurtki/133050058-kurtka-dlya-divchinki',
      thumbnail: 'https://image-thumbs.shafastatic.net/1329072819_310_430',
      name: 'Куртка для дівчинки',
      price: 300,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 31,
      isLiked: false,
      __typename: 'Product',
      brand: null,
      catalogSlug: 'forWomen',
      isNew: false,
      sizes: [
        {
          id: 69,
          name: '10 / 140',
          __typename: 'Size'
        },
        {
          id: 68,
          name: '9 / 134',
          __typename: 'Size'
        },
        {
          id: 70,
          name: '11 / 146',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 133147713,
      url: '/uk/women/shtany/bryuki/133147713-peserico-bryuki',
      thumbnail: 'https://image-thumbs.shafastatic.net/1331132085_310_430',
      name: 'Peserico  брюки',
      price: 1999,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 7,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 46081,
        name: 'Peserico',
        __typename: 'Brand'
      },
      catalogSlug: 'forWomen',
      isNew: false,
      sizes: [
        {
          id: 3,
          name: '36 / S / 44',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 33114405,
      url: '/uk/women/aksessuary/shapki/panamy/33114405-panama-panamka-shlyapa-shapka-chernaya-s-kolcami-i-bulavkoy',
      thumbnail: 'https://image-thumbs.shafastatic.net/134865357_310_430',
      name: 'Панама панамка стильна тренд чорна з пірсингом з булавкою нова',
      price: 247,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 505,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 45421,
        name: 'Honey Fashion Accessories',
        __typename: 'Brand'
      },
      catalogSlug: 'forWomen',
      isNew: true,
      sizes: [
        {
          id: 327,
          name: 'One size',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: ['FREE_SHIPPING'],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 127136028,
      url: '/uk/kids/dlya-malchikov/rubashka/127136028-orochka',
      thumbnail: 'https://image-thumbs.shafastatic.net/1185621085_310_430',
      name: 'Сорочка',
      price: 310,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 39,
      isLiked: false,
      __typename: 'Product',
      brand: null,
      catalogSlug: 'forMen',
      isNew: true,
      sizes: [
        {
          id: 61,
          name: '2 / 92',
          __typename: 'Size'
        },
        {
          id: 62,
          name: '3 / 98',
          __typename: 'Size'
        },
        {
          id: 63,
          name: '4 / 104',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: true,
      ownerHasRecentActivity: true,
      tags: ['UKRBRAND'],
      rating: 5,
      ratingAmount: 4,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 133227622,
      url: '/uk/women/shtany/dzhinsy/133227622-dzhinsi-stretch',
      thumbnail: 'https://image-thumbs.shafastatic.net/1333012989_310_430',
      name: 'Джинси стретч',
      price: 250,
      oldPrice: 300,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: 17,
      likes: 19,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 49,
        name: 'F&F',
        __typename: 'Brand'
      },
      catalogSlug: 'forWomen',
      isNew: false,
      sizes: [
        {
          id: 52,
          name: '29 / M',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: ['FREE_SHIPPING'],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 105048392,
      url: '/uk/cosmetics/dlya-volos/sprei/105048392-color-wow-dream-coat-spray-spryay-dlya-vyravnivaya-volos',
      thumbnail: 'https://image-thumbs.shafastatic.net/633325022_310_430',
      name: 'Color wow dream coat spray спрей для вирівнювання волосся',
      price: 500,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 60,
      isLiked: false,
      __typename: 'Product',
      brand: null,
      catalogSlug: 'beautyAndHealth',
      isNew: true,
      sizes: [
        {
          id: 11,
          name: 'One size',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 122892760,
      url: '/uk/women/aksessuary/ukrasheniya-i-chasy/kolca/122892760-kilce-kolco-kolechko-sriblo-s925-akcentne-sribne-stilne-modne-nove',
      thumbnail: 'https://image-thumbs.shafastatic.net/1054046977_310_430',
      name: 'Кільце кольцо колечко срібло s925 акцентне срібне стильне модне нове',
      price: 169,
      oldPrice: 220,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: 24,
      likes: 66,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 3275,
        name: 'New Style',
        __typename: 'Brand'
      },
      catalogSlug: 'forWomen',
      isNew: true,
      sizes: [
        {
          id: 11,
          name: 'One size',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: ['FREE_SHIPPING'],
      rating: 5,
      ratingAmount: 2,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 133263910,
      url: '/uk/women/verhnyaya-odezhda/pidzhaki-i-zhakety/133263910-korotkiy-ukorocheniy-teksturovaniy-tvidoviy-blyayzer-pidzhak-zara',
      thumbnail: 'https://image-thumbs.shafastatic.net/1333623270_310_430',
      name: 'Короткий, укорочений текстурований , твідовий блейзер, піджак zara',
      price: 850,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 43,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 183,
        name: 'ZARA',
        __typename: 'Brand'
      },
      catalogSlug: 'forWomen',
      isNew: false,
      sizes: [
        {
          id: 3,
          name: '36 / S / 44',
          __typename: 'Size'
        },
        {
          id: 4,
          name: '38 / M / 46',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 116019339,
      url: '/uk/cosmetics/telo-i-vanna/kosmetika/mylo/116019339-ridke-milo-rituals-the-ritual-of-sakura-hand-wash-300-ml',
      thumbnail: 'https://image-thumbs.shafastatic.net/1334742058_310_430',
      name: 'Рідке мило - rituals the ritual of sakura/karma/mehr hand wash 300 ml',
      price: 400,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 21,
      isLiked: false,
      __typename: 'Product',
      brand: null,
      catalogSlug: 'beautyAndHealth',
      isNew: true,
      sizes: [
        {
          id: 11,
          name: 'One size',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: 5,
      ratingAmount: 1,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 83907068,
      url: '/uk/women/aksessuary/ukrasheniya-i-chasy/sergi/83907068-trendovye-serezhki-sergi-kaff-kafy',
      thumbnail: 'https://image-thumbs.shafastatic.net/390694337_310_430',
      name: 'Пара (2 шт.), трендові сережки, сережки, кафф, кафи,',
      price: 180,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 45,
      isLiked: false,
      __typename: 'Product',
      brand: null,
      catalogSlug: 'forWomen',
      isNew: true,
      sizes: [
        {
          id: 11,
          name: 'One size',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 133157091,
      url: '/uk/home/hranenie-veshchej/korobki/133157091-chemodan-velikiy',
      thumbnail: 'https://image-thumbs.shafastatic.net/1331283930_310_430',
      name: 'Чемодан великий',
      price: 660,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 90,
      isLiked: false,
      __typename: 'Product',
      brand: null,
      catalogSlug: 'sportsAndRecreation',
      isNew: false,
      sizes: [
        {
          id: 11,
          name: 'One size',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: true,
      ownerHasRecentActivity: true,
      tags: ['UKRBRAND'],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 115919894,
      url: '/uk/women/yubki/midi/115919894-shovkova-spidnicya-midi',
      thumbnail: 'https://image-thumbs.shafastatic.net/852044299_310_430',
      name: 'Шовкова спідниця миди',
      price: 500,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 1286,
      isLiked: false,
      __typename: 'Product',
      brand: null,
      catalogSlug: 'forWomen',
      isNew: true,
      sizes: [
        {
          id: 1,
          name: '32 / XXS / 40',
          __typename: 'Size'
        },
        {
          id: 2,
          name: '34 / XS / 42',
          __typename: 'Size'
        },
        {
          id: 3,
          name: '36 / S / 44',
          __typename: 'Size'
        },
        {
          id: 4,
          name: '38 / M / 46',
          __typename: 'Size'
        },
        {
          id: 5,
          name: '40 /  L / 48',
          __typename: 'Size'
        },
        {
          id: 6,
          name: '42 /  XL / 50',
          __typename: 'Size'
        },
        {
          id: 7,
          name: '44 / XXL / 52',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: 5,
      ratingAmount: 29,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 133224160,
      url: '/uk/women/kofty/svitshoty/133224160-svitshot',
      thumbnail: 'https://image-thumbs.shafastatic.net/1332940644_310_430',
      name: 'Світшот',
      price: 299,
      oldPrice: 400,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: 26,
      likes: 41,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 14842,
        name: 'Fruit Of The Loom',
        __typename: 'Brand'
      },
      catalogSlug: 'forWomen',
      isNew: false,
      sizes: [
        {
          id: 5,
          name: '40 /  L / 48',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: ['FREE_SHIPPING'],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 132574577,
      url: '/uk/women/mayki-i-futbolki/topy/132574577-zhenskiy-krasivyy-fioletovyy-top',
      thumbnail: 'https://image-thumbs.shafastatic.net/1318404978_310_430',
      name: 'Женский красивый фиолетовый топ',
      price: 100,
      oldPrice: 150,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: 34,
      likes: 64,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 45818,
        name: 'Goldi',
        __typename: 'Brand'
      },
      catalogSlug: 'forWomen',
      isNew: false,
      sizes: [
        {
          id: 3,
          name: '36 / S / 44',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NOT_STARTED',
        date: '2023-09-29',
        price: '88',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: true,
      ownerHasRecentActivity: true,
      tags: ['UKRBRAND'],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 107892055,
      url: '/uk/kids/dlya-devochek/sapogi-i-botinki/107892055-kozhanye-uggi-detskie',
      thumbnail: 'https://image-thumbs.shafastatic.net/682569995_310_430',
      name: 'Шкіряні уги дитячі',
      price: 920,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 6,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 46906,
        name: 'ITTS',
        __typename: 'Brand'
      },
      catalogSlug: 'forWomen',
      isNew: true,
      sizes: [
        {
          id: 92,
          name: '26',
          __typename: 'Size'
        },
        {
          id: 93,
          name: '27',
          __typename: 'Size'
        },
        {
          id: 94,
          name: '28',
          __typename: 'Size'
        },
        {
          id: 95,
          name: '29',
          __typename: 'Size'
        },
        {
          id: 96,
          name: '30',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 133064016,
      url: '/uk/women/platya/mini/133064016-nova-chorna-suknya-fb-sister',
      thumbnail: 'https://image-thumbs.shafastatic.net/1329287730_310_430',
      name: 'Нова чорна сукня fb sister',
      price: 250,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 75,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 4690,
        name: 'FB Sister',
        __typename: 'Brand'
      },
      catalogSlug: 'kidsProducts',
      isNew: true,
      sizes: [
        {
          id: 4,
          name: '38 / M / 46',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NOT_STARTED',
        date: '2023-09-25',
        price: '220',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 126579724,
      url: '/uk/women/aksessuary/sumki/zhenskiye/126579724-jw-pei',
      thumbnail: 'https://image-thumbs.shafastatic.net/1166506294_310_430',
      name: 'Сумка',
      price: 1700,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 41,
      isLiked: false,
      __typename: 'Product',
      brand: null,
      catalogSlug: 'forWomen',
      isNew: true,
      sizes: [
        {
          id: 11,
          name: 'One size',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 132915186,
      url: '/uk/men/aksessuari/galstuki-i-babochki/132915186-kravatka',
      thumbnail: 'https://image-thumbs.shafastatic.net/1325719835_310_430',
      name: 'Краватка в стилі гаррі поттер',
      price: 89,
      oldPrice: 99,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: 11,
      likes: 7,
      isLiked: false,
      __typename: 'Product',
      brand: null,
      catalogSlug: 'forMen',
      isNew: false,
      sizes: [
        {
          id: 11,
          name: 'One size',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NOT_STARTED',
        date: '2023-09-27',
        price: '76',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 133153476,
      url: '/uk/women/platya/mini/133153476-plattya',
      thumbnail: 'https://image-thumbs.shafastatic.net/1331225626_310_430',
      name: 'Плаття',
      price: 265,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 35,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 183,
        name: 'ZARA',
        __typename: 'Brand'
      },
      catalogSlug: 'forWomen',
      isNew: false,
      sizes: [
        {
          id: 2,
          name: '34 / XS / 42',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 108594311,
      url: '/uk/women/rubashki-i-bluzy/vyshivanki/108594311-kashemirovyy-zhaket-s-vyshivkoy-bez-zastezhek-teplyy-zhaket',
      thumbnail: 'https://image-thumbs.shafastatic.net/692252372_310_430',
      name: 'Кашеміровий жакет з вишивкою без застібок, теплий жакет, вишита рубашка, піджак з вишивкою, вишиванка',
      price: 1150,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 253,
      isLiked: false,
      __typename: 'Product',
      brand: null,
      catalogSlug: 'forWomen',
      isNew: true,
      sizes: [
        {
          id: 2,
          name: '34 / XS / 42',
          __typename: 'Size'
        },
        {
          id: 3,
          name: '36 / S / 44',
          __typename: 'Size'
        },
        {
          id: 4,
          name: '38 / M / 46',
          __typename: 'Size'
        },
        {
          id: 5,
          name: '40 /  L / 48',
          __typename: 'Size'
        },
        {
          id: 6,
          name: '42 /  XL / 50',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: ['FREE_SHIPPING'],
      rating: 5,
      ratingAmount: 1,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 133254624,
      url: '/uk/women/verhnyaya-odezhda/kurtki/133254624-zhenskaya-zimnyaya-kurtochka-ukorochennaya-kurtka-tommy-jeans-m',
      thumbnail: 'https://image-thumbs.shafastatic.net/1333429806_310_430',
      name: 'Жіноча зимова курточка укорочена куртка tommy jeans м розмір',
      price: 2350,
      oldPrice: null,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: null,
      likes: 24,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 159,
        name: 'Tommy Hilfiger',
        __typename: 'Brand'
      },
      catalogSlug: 'forWomen',
      isNew: false,
      sizes: [
        {
          id: 4,
          name: '38 / M / 46',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: ['FREE_SHIPPING'],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  },
  {
    node: {
      id: 132970104,
      url: '/uk/women/shtany/dzhinsy/132970104-molochni-dzhinsi-mom-vid-mango-u-rozmiri-m',
      thumbnail: 'https://image-thumbs.shafastatic.net/1327220463_310_430',
      name: 'Молочні джинси mom від mango у розмірі m',
      price: 320,
      oldPrice: 350,
      statusTitle: 'AVAILABLE',
      isTop: false,
      discountPercent: 9,
      likes: 33,
      isLiked: false,
      __typename: 'Product',
      brand: {
        id: 94,
        name: 'Mango',
        __typename: 'Brand'
      },
      catalogSlug: 'forWomen',
      isNew: false,
      sizes: [
        {
          id: 333,
          name: '28 / M',
          __typename: 'Size'
        },
        {
          id: 52,
          name: '29 / M',
          __typename: 'Size'
        }
      ],
      saleLabel: {
        status: 'SALE_NONE',
        date: '2023-09-24',
        price: '0',
        __typename: 'SaleLabel'
      },
      freeDeliveryServices: null,
      isUkrainian: false,
      ownerHasRecentActivity: true,
      tags: [],
      rating: null,
      ratingAmount: null,
      comments: []
    },
    __typename: 'FeedEdge'
  }
];
