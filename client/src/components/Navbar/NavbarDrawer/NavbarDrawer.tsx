import { Drawer } from 'antd';
import { useState } from 'react';
import { BurgerIcon } from '../../../ui/icons/icons';

export function NavbarDrawer() {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);

  const handleDrawerClick = () => {
    setIsDrawerOpen(true);
  };

  const handleDrawerClose = () => {
    setIsDrawerOpen(false);
  };
  return (
    <>
      {/* eslint-disable-next-line jsx-a11y/control-has-associated-label */}
      <button type="button" className="h-7 w-7 border-0 bg-transparent pt-1" onClick={handleDrawerClick}>
        <BurgerIcon />
      </button>
      <Drawer title="Basic Drawer" placement="left" onClose={handleDrawerClose} open={isDrawerOpen}>
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Drawer>
    </>
  );
}
