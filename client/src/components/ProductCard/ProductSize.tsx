import { useProductCardContext } from './ProductCardContext';

function ProductSize() {
  const {
    product: { sizes }
  } = useProductCardContext();

  return (
    <div className="flex items-center">
      <p className="my-0 mr-1 text-sm font-medium">
        {sizes[0].name} <span className="font-normal">{sizes.length > 1 && `i ще ${sizes.length - 1}`}</span>
      </p>
    </div>
  );
}

export default ProductSize;
