/* eslint-disable no-use-before-define */
import { useAppDispatch } from '#/libs/hooks/redux';
import { Product } from '#/libs/types/Products/Product.type';
import { Tag } from '#/libs/types/Products/Tag.type';
import { addProduct } from '#/store/reducers/ProductSlice';
import { Button, Checkbox, Form, Input, Modal, notification } from 'antd';
import { useFormik } from 'formik';
import React, { useState } from 'react';
import * as Yup from 'yup';

interface AddProductProps {
  visible: boolean;
  setVisible: React.Dispatch<React.SetStateAction<boolean>>;
}

const AddProductSchema = Yup.object().shape({
  name: Yup.string().required('Product name is required'),
  price: Yup.number().required('Price is required').min(0, 'Price must be greater than or equal to 0'),
  thumbnail: Yup.string().url('Thumbnail has to be URL').required('Thumbnail URL is required'),
  oldPrice: Yup.number()
    .min(0, 'Old price must be greater than or equal to 0')
    .test('oldPrice', 'Old price cannot be lower than the price', function (oldPrice) {
      const { price } = this.parent;
      return oldPrice === undefined || oldPrice >= price;
    }),
  statusTitle: Yup.string().required('Status title is required'),
  isTop: Yup.boolean().required('Please select whether it is top or not'),
  likes: Yup.number().min(0, 'Likes must be greater than or equal to 0'),
  isLiked: Yup.boolean().required('Please select whether it is liked or not'),
  catalogSlug: Yup.string().required('Catalog slug is required'),
  isNew: Yup.boolean().required('Please select whether it is new or not'),
  sizes: Yup.array()
    .of(
      Yup.object().shape({
        id: Yup.number().required('Size ID is required'),
        name: Yup.string().required('Size name is required')
      })
    )
    .required('At least one size is required'),
  freeDeliveryServices: Yup.boolean(),
  isUkrainian: Yup.boolean().required('Please select whether it is Ukrainian or not'),
  tags: Yup.array()
    .of(Yup.string().oneOf(['FREE_SHIPPING', 'UKRBRAND'] as Tag[]))
    .required('At least one tag is required')
});

function AddProductModal({ visible, setVisible }: AddProductProps) {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [api, contextHolder] = notification.useNotification();
  const dispatch = useAppDispatch();

  const formik = useFormik({
    initialValues: {
      name: '',
      price: 0,
      thumbnail: '',
      oldPrice: 0,
      statusTitle: '',
      isTop: false,
      likes: 0,
      isLiked: false,
      catalogSlug: '',
      isNew: false,
      sizes: [{ id: 1, name: '', __typename: '' }],
      freeDeliveryServices: false,
      isUkrainian: false,
      tags: [] as Tag[]
    },
    validationSchema: AddProductSchema,
    onSubmit: (
      values: Pick<
        Product,
        | 'name'
        | 'price'
        | 'thumbnail'
        | 'oldPrice'
        | 'statusTitle'
        | 'isTop'
        | 'likes'
        | 'isLiked'
        | 'catalogSlug'
        | 'isNew'
        | 'sizes'
        | 'freeDeliveryServices'
        | 'isUkrainian'
        | 'tags'
      >
    ) => {
      setIsLoading(true);
      // Perform your product creation logic here
      console.log(values);
      dispatch(addProduct(values));
      api.success({
        message: 'Success',
        description: 'Product created successfully!',
        placement: 'topRight'
      });
      setIsLoading(false);
      onCancel();
    }
  });

  const onCancel = () => {
    formik.resetForm();
    setVisible(false);
  };

  const addNewSize = () => {
    formik.setFieldValue('sizes', [
      ...formik.values.sizes,
      { id: formik.values.sizes.length + 1, name: '', __typename: '' }
    ]);
  };

  return (
    <>
      {contextHolder}
      <Modal title="Add Product" open={visible} onCancel={onCancel} footer={[]}>
        <Form layout="vertical" onFinish={formik.handleSubmit}>
          <Form.Item
            label="Product Name"
            validateStatus={formik.errors.name && formik.touched.name ? 'error' : ''}
            help={formik.errors.name && formik.touched.name ? formik.errors.name : ''}
          >
            <Input name="name" value={formik.values.name} onChange={formik.handleChange} onBlur={formik.handleBlur} />
          </Form.Item>
          <Form.Item
            label="Price"
            validateStatus={formik.errors.price && formik.touched.price ? 'error' : ''}
            help={formik.errors.price && formik.touched.price ? formik.errors.price : ''}
          >
            <Input
              type="number"
              name="price"
              value={formik.values.price}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </Form.Item>
          <Form.Item
            label="Thumbnail URL"
            validateStatus={formik.errors.thumbnail && formik.touched.thumbnail ? 'error' : ''}
            help={formik.errors.thumbnail && formik.touched.thumbnail ? formik.errors.thumbnail : ''}
          >
            <Input
              name="thumbnail"
              value={formik.values.thumbnail}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </Form.Item>
          <Form.Item
            label="Old Price"
            validateStatus={formik.errors.oldPrice && formik.touched.oldPrice ? 'error' : ''}
            help={formik.errors.oldPrice && formik.touched.oldPrice ? formik.errors.oldPrice : ''}
          >
            <Input
              type="number"
              name="oldPrice"
              value={formik.values.oldPrice as number}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </Form.Item>
          <Form.Item
            label="Status Title"
            validateStatus={formik.errors.statusTitle && formik.touched.statusTitle ? 'error' : ''}
            help={formik.errors.statusTitle && formik.touched.statusTitle ? formik.errors.statusTitle : ''}
          >
            <Input
              name="statusTitle"
              value={formik.values.statusTitle}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </Form.Item>
          <Form.Item
            label="Is Top"
            validateStatus={formik.errors.isTop && formik.touched.isTop ? 'error' : ''}
            help={formik.errors.isTop && formik.touched.isTop ? formik.errors.isTop : ''}
          >
            <Checkbox
              name="isTop"
              checked={formik.values.isTop}
              onChange={(e) => formik.setFieldValue('isTop', e.target.checked)}
            >
              Is Top
            </Checkbox>
          </Form.Item>

          <Form.Item
            label="Likes"
            validateStatus={formik.errors.likes && formik.touched.likes ? 'error' : ''}
            help={formik.errors.likes && formik.touched.likes ? formik.errors.likes : ''}
          >
            <Input
              type="number"
              name="likes"
              value={formik.values.likes}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </Form.Item>
          <Form.Item
            label="Is Liked"
            validateStatus={formik.errors.isLiked && formik.touched.isLiked ? 'error' : ''}
            help={formik.errors.isLiked && formik.touched.isLiked ? formik.errors.isLiked : ''}
          >
            <Checkbox
              name="isLiked"
              checked={formik.values.isLiked}
              onChange={(e) => formik.setFieldValue('isLiked', e.target.checked)}
            >
              Is Liked
            </Checkbox>
          </Form.Item>
          <Form.Item
            label="Catalog Slug"
            validateStatus={formik.errors.catalogSlug && formik.touched.catalogSlug ? 'error' : ''}
            help={formik.errors.catalogSlug && formik.touched.catalogSlug ? formik.errors.catalogSlug : ''}
          >
            <Input
              name="catalogSlug"
              value={formik.values.catalogSlug}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </Form.Item>
          <Form.Item
            label="Is New"
            validateStatus={formik.errors.isNew && formik.touched.isNew ? 'error' : ''}
            help={formik.errors.isNew && formik.touched.isNew ? formik.errors.isNew : ''}
          >
            <Checkbox
              name="isNew"
              checked={formik.values.isNew}
              onChange={(e) => formik.setFieldValue('isNew', e.target.checked)}
            >
              Is New
            </Checkbox>
          </Form.Item>
          <Form.Item
            label="Sizes"
            validateStatus={formik.errors.sizes && formik.touched.sizes ? 'error' : ''}
            help={
              formik.errors.sizes && formik.touched.sizes && Array.isArray(formik.errors.sizes)
                ? formik.errors.sizes.map((item) => (typeof item === 'string' ? item : Object.values(item).join(' ,')))
                : ''
            }
          >
            {formik.values.sizes.map((size, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <div key={size.id} style={{ marginBottom: '8px' }}>
                <Input
                  name={`sizes[${index}].id`}
                  value={size.id}
                  placeholder="Size ID"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                <Input
                  name={`sizes[${index}].name`}
                  value={size.name}
                  placeholder="Size Name"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            ))}
            <Button type="dashed" onClick={addNewSize} style={{ width: '100%' }}>
              + Add New Size
            </Button>
          </Form.Item>
          <Form.Item
            label="Free Delivery Services"
            validateStatus={formik.errors.freeDeliveryServices && formik.touched.freeDeliveryServices ? 'error' : ''}
            help={
              formik.errors.freeDeliveryServices && formik.touched.freeDeliveryServices
                ? formik.errors.freeDeliveryServices
                : ''
            }
          >
            <Checkbox
              name="freeDeliveryServices"
              checked={!!formik.values.freeDeliveryServices}
              onChange={(e) => formik.setFieldValue('freeDeliveryServices', e.target.checked)}
            >
              Free Delivery Services
            </Checkbox>
          </Form.Item>
          <Form.Item
            label="Is Ukrainian"
            validateStatus={formik.errors.isUkrainian && formik.touched.isUkrainian ? 'error' : ''}
            help={formik.errors.isUkrainian && formik.touched.isUkrainian ? formik.errors.isUkrainian : ''}
          >
            <Checkbox
              name="isUkrainian"
              checked={formik.values.isUkrainian}
              onChange={(e) => formik.setFieldValue('isUkrainian', e.target.checked)}
            >
              Is Ukrainian
            </Checkbox>
          </Form.Item>
          <Form.Item
            label="Tags"
            validateStatus={formik.errors.tags && formik.touched.tags ? 'error' : ''}
            help={formik.errors.tags && formik.touched.tags ? formik.errors.tags : ''}
          >
            <Checkbox.Group
              options={['FREE_SHIPPING', 'UKRBRAND']}
              name="tags"
              value={formik.values.tags}
              onChange={(values) => formik.setFieldValue('tags', values)}
            />
          </Form.Item>
          <div className="flex justify-end gap-4">
            <Button key="back" onClick={onCancel}>
              Cancel
            </Button>
            <Button loading={isLoading} key="submit" htmlType="submit" type="primary">
              Add Product
            </Button>
          </div>
        </Form>
      </Modal>
    </>
  );
}

export default AddProductModal;
