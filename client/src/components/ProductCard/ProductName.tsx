import { PRODUCT_ROUTE_$ID } from '#/libs/constants/routes';
import { cn } from '#/libs/helpers/cn';
import { getRouteWithId } from '#/libs/helpers/getRouteWithId';
import { ClassNamed } from '#/libs/types/ClassNamed/ClassNamed.type';
import AppLink from '../AppLink/AppLink';
import { useProductCardContext } from './ProductCardContext';

function ProductName({ className }: ClassNamed) {
  const {
    product: { name, id }
  } = useProductCardContext();
  return (
    <AppLink
      href={getRouteWithId(PRODUCT_ROUTE_$ID, id)}
      title={name}
      className={cn(
        'w-30 my-0 box-content line-clamp-2 overflow-hidden truncate text-ellipsis text-sm sm:w-36',
        className
      )}
    />
  );
}

export default ProductName;
