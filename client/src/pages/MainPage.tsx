import AddProductModal from '#/components/MainPage/AddProductModal';
import { useSearch } from '#/libs/hooks/useSearch';
import { useState } from 'react';
import { useParams, useSearchParams } from 'react-router-dom';
import { TransitionGroup } from 'react-transition-group';
import ProductCard from '../components/ProductCard/ProductCard';
import { useAppSelector } from '../libs/hooks/redux';

export default function MainPage() {
  const { products } = useAppSelector((state) => state.productReducer);
  const [addProductModalActive, setAddProductModalActive] = useState(false);
  const [searchParams] = useSearchParams({ query: '', category: '' });
  const [slice, setSlice] = useState<number>(20);

  const { category } = useParams();

  const querySearchParam = searchParams.get('query') || '';
  const categoryParam = category || '';

  const filteredProductsByName = useSearch(
    querySearchParam,
    products.map(({ node }) => node),
    'name'
  );

  const handleAddButton = () => {
    setSlice((prev) => prev + 1);
  };

  const handleModalButton = () => {
    setAddProductModalActive(true);
  };

  const filteredProducts = useSearch(categoryParam, filteredProductsByName, 'catalogSlug');

  const firstOtherFilteredProducts = filteredProducts.slice(0, slice);

  return (
    <main className="mx-auto w-full sm:w-11/12 lg:w-11/12 xl:w-10/12 2xl:w-2/3">
      <h2 className="ml-3">Оголошення</h2>
      <TransitionGroup>
        <ul className="grid list-none grid-cols-2 gap-3 px-3 md:grid-cols-3 lg:grid-cols-5 lg:p-5">
          {firstOtherFilteredProducts.map((item) => (
            <li key={item.id}>
              <ProductCard
                product={item}
                image={
                  <ProductCard.Image
                    bottom={<ProductCard.Brand />}
                    top={
                      <ProductCard.TagContainer>
                        <ProductCard.Tag.FreeDelivery />
                        <ProductCard.Tag.Ukrainian />
                      </ProductCard.TagContainer>
                    }
                  />
                }
                info={
                  <ProductCard.Info>
                    <ProductCard.Price />
                    <ProductCard.Discount />
                    <ProductCard.Name />
                    <ProductCard.Size />
                  </ProductCard.Info>
                }
              />
            </li>
          ))}
          {filteredProducts.length !== slice && (
            <li className="flex h-full min-h-[377px] flex-col items-center justify-center gap-1  ">
              <button
                type="button"
                onClick={handleAddButton}
                className="h-1/2 w-full cursor-pointer rounded-lg border-0 bg-theme-green px-[77px] text-6xl font-bold"
              >
                +
              </button>
              <button
                type="button"
                onClick={handleModalButton}
                className="h-1/2 w-full cursor-pointer rounded-lg border-0 bg-green-400 text-3xl font-bold"
              >
                modal
              </button>
            </li>
          )}
        </ul>
      </TransitionGroup>
      <AddProductModal setVisible={setAddProductModalActive} visible={addProductModalActive} />
    </main>
  );
}
