import { useAppDispatch } from '../../libs/hooks/redux';
import { likeProduct, unlikeProduct } from '../../store/reducers/ProductSlice';
import { ClickedHeartIcon, HeartIcon } from '../../ui/icons/icons';
import { useProductCardContext } from './ProductCardContext';

function ProductLike() {
  const { product } = useProductCardContext();

  const { likes, isLiked } = product;
  const dispatch = useAppDispatch();
  const handleHeartClick = () => {
    dispatch(isLiked ? unlikeProduct(product) : likeProduct(product));
  };

  return (
    <button type="button" className="mt-1 flex cursor-pointer border-0 bg-transparent p-0" onClick={handleHeartClick}>
      <div className="flex items-center">
        <p className="my-0 mr-0 text-xs font-normal sm:mr-1">{likes}</p>
        {isLiked ? <ClickedHeartIcon /> : <HeartIcon />}
      </div>
    </button>
  );
}

export default ProductLike;
