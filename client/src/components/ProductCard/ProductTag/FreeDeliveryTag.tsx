import { TruckIcon } from '#/ui/icons/icons';
import ProductTag from './ProductTag';

function FreeDeliveryTag() {
  return (
    <>
      <ProductTag icon={<TruckIcon />} tag="FREE_SHIPPING" />
    </>
  );
}

export default FreeDeliveryTag;
