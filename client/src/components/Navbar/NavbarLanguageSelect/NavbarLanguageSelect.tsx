import { Typography } from 'antd';
import { ClassValue } from 'clsx';
import { useState } from 'react';
import { cn } from '../../../libs/helpers/cn';

const languages = ['UK', 'EN'];
const { Text } = Typography;

interface NavbarLanguageSelectProps {
  className: ClassValue;
}

export function NavbarLanguageSelect({ className }: NavbarLanguageSelectProps) {
  // TODO: Replace with redux action
  const [selectedLanguage, setSelectedLanguage] = useState<string>('UK');
  const handleLanguageSelect = (language: string) => {
    setSelectedLanguage(language);
  };

  return (
    <Text className={cn(className)}>
      {languages.map((language, index) => (
        <span key={language}>
          <Text
            strong={selectedLanguage === language}
            onClick={() => handleLanguageSelect(language)}
            style={{ cursor: 'pointer' }}
          >
            {language}
          </Text>
          {index < languages.length - 1 && <Text> | </Text>}
        </span>
      ))}
    </Text>
  );
}
