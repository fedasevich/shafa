import { ADMIN_ROUTE } from '#/libs/constants/routes';
import { cn } from '#/libs/helpers/cn';
import { Button } from 'antd';
import { ClassValue } from 'clsx';
import { useNavigate } from 'react-router-dom';

interface NavbarButtonProps {
  className: ClassValue;
}

export function NavbarButton({ className }: NavbarButtonProps) {
  const navigate = useNavigate();

  const handleButtonClick = () => {
    navigate(ADMIN_ROUTE);
  };

  return (
    <Button
      className={cn(
        'border-1 h-10 self-start border-black bg-transparent py-0 text-center text-xs lg:self-center',
        className
      )}
      onClick={handleButtonClick}
    >
      Admin
    </Button>
  );
}
