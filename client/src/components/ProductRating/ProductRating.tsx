import { EmptyStarIcon, StarIcon } from '#/ui/icons/icons';

interface ProductRatingProps {
  filled: number;
}

export function ProductRating({ filled }: ProductRatingProps) {
  return (
    <div>
      {Array.from({ length: 5 }, (_, index) => (
        <button
          type="button"
          className="cursor-pointer border-0 bg-transparent p-0"
          key={index}
          style={{ display: 'inline-block' }}
        >
          {index + 1 <= filled ? <StarIcon /> : <EmptyStarIcon />}
        </button>
      ))}
    </div>
  );
}
