import {
  ADMIN_ROUTE,
  CART_ROUTE,
  PRIVACY_POLICY_ROUTE,
  PRODUCT_ROUTE_$ID,
  SHOP_ROUTE,
  SHOP_ROUTE_CATEGORY,
  TERMS_OF_SERVICE_ROUTE
} from './libs/constants/routes';
import { AdminPage } from './pages/AdminPage';
import CartPage from './pages/CartPage';
import MainPage from './pages/MainPage';
import { PrivacyPolicyPage } from './pages/PrivacyPolicyPage';
import ProductPage from './pages/ProductPage';
import { TermsOfServicePage } from './pages/TermsOfServicePage';

export const publicRoutes = [
  {
    path: SHOP_ROUTE,
    Component: MainPage
  },
  {
    path: SHOP_ROUTE_CATEGORY,
    Component: MainPage
  },
  {
    path: CART_ROUTE,
    Component: CartPage
  },
  {
    path: PRODUCT_ROUTE_$ID,
    Component: ProductPage
  },
  {
    path: PRIVACY_POLICY_ROUTE,
    Component: PrivacyPolicyPage
  },
  {
    path: TERMS_OF_SERVICE_ROUTE,
    Component: TermsOfServicePage
  },
  {
    path: ADMIN_ROUTE,
    Component: AdminPage
  }
];
