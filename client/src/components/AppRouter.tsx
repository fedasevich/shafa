import useHistoricNavigate from '#/libs/hooks/useHistoricNavigate';
import { VisitedRoute } from '#/libs/types/Route/VisitedRoute';
import { createContext } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { SHOP_ROUTE } from '../libs/constants/routes';
import { publicRoutes } from '../routes';
import { DebugWindow } from './DebugWindow';

export const HistoryContext = createContext<VisitedRoute[] | null>(null);

function AppRouter() {
  const { visitedRoutes } = useHistoricNavigate();
  return (
    <>
      <HistoryContext.Provider value={visitedRoutes}>
        <Routes>
          {publicRoutes.map(({ path, Component }) => (
            <Route
              key={path}
              path={path}
              element={
                <>
                  <Component />
                  <DebugWindow />
                </>
              }
            />
          ))}
          <Route path="*" element={<Navigate to={SHOP_ROUTE} replace />} />
        </Routes>
      </HistoryContext.Provider>
    </>
  );
}

export default AppRouter;
