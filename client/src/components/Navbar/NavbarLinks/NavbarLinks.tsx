import { Typography } from 'antd';
import { ClassValue } from 'clsx';
import { useState } from 'react';
import { cn } from '../../../libs/helpers/cn';
import { useAppDispatch, useAppSelector } from '../../../libs/hooks/redux';
import { logOut, login } from '../../../store/reducers/UserSlice';
import AppLink from '../../AppLink/AppLink';
import AuthModal from './AuthModal';

const { Text } = Typography;

interface NavbarLinksProps {
  className: ClassValue;
}

export function NavbarLinks({ className }: NavbarLinksProps) {
  const [authModalActive, setAuthModalActive] = useState(false);

  const { user } = useAppSelector((state) => state.userReducer);
  const dispatch = useAppDispatch();

  const handleLogin = () => {
    setAuthModalActive(true);
    dispatch(login({ isAuth: true, userCurrency: user.userCurrency }));
  };

  const handleLogOut = () => {
    setAuthModalActive(true);

    dispatch(logOut({ isAuth: false, userCurrency: user.userCurrency }));
  };

  return (
    <div className={cn(className)}>
      {user?.isAuth ? (
        // eslint-disable-next-line jsx-a11y/control-has-associated-label
        <button className="border-0 bg-transparent" onClick={handleLogOut} type="button">
          <AppLink title="Вийти" href="/" />
        </button>
      ) : (
        <button className="border-0 bg-transparent" onClick={handleLogin} type="button">
          <AppLink title="Вхід" href="/" />
          <Text> | </Text>
          <AppLink title="Реєстрація" href="/" />
        </button>
      )}
      <AuthModal visible={authModalActive} setVisible={setAuthModalActive} />
    </div>
  );
}
