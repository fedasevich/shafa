import useHistoricNavigate from '#/libs/hooks/useHistoricNavigate';
import { Affix } from 'antd';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { SHOP_ROUTE } from '../../libs/constants/routes';
import { cn } from '../../libs/helpers/cn';
import { LogoIcon } from '../../ui/icons/icons';
import { NavbarButton } from './NavbarButton/NavbarButton';
import { NavbarCart } from './NavbarCart/NavbarCart';
import { NavbarCurrencySelect } from './NavbarCurrencySelect/NavbarCurrencySelect';
import { NavbarDrawer } from './NavbarDrawer/NavbarDrawer';
import { NavbarLinks } from './NavbarLinks/NavbarLinks';
import { NavbarSearch } from './NavbarSearch/NavbarSearch';

export function Navbar() {
  const [isResponsiveSearchOpen, setIsResponsiveSearchOpen] = useState(false);

  const { onLinkClick } = useHistoricNavigate();

  const handleLogoLinkClick = () => {
    onLinkClick(SHOP_ROUTE);
  };

  return (
    <Affix className="bg-gray-200">
      <div
        className={cn(
          ' flex justify-center bg-gray-200 px-2 py-3 lg:px-2 lg:py-5',
          isResponsiveSearchOpen && 'py-[6.5px] lg:py-5'
        )}
      >
        <div className="flex w-full items-center justify-between gap-3 align-middle lg:justify-center xl:w-10/12 2xl:w-2/3">
          <div className={cn('mr-18 flex', isResponsiveSearchOpen && 'hidden lg:block')}>
            <div className="lg:hidden">
              <NavbarDrawer />
            </div>
            <Link to={SHOP_ROUTE} onClick={handleLogoLinkClick}>
              <LogoIcon />
            </Link>
          </div>
          <div
            className={cn(
              'flex flex-row-reverse justify-end gap-3 lg:w-full lg:flex-row lg:items-center lg:justify-evenly lg:gap-5',
              isResponsiveSearchOpen && 'w-full'
            )}
          >
            <NavbarSearch setIsOpen={setIsResponsiveSearchOpen} isOpen={isResponsiveSearchOpen} />
            <NavbarButton className="hidden lg:block" />
            <NavbarCurrencySelect className="hidden lg:block" />
            {/* <NavbarLanguageSelect className="hidden lg:block" /> */}
            <NavbarCart className={isResponsiveSearchOpen && 'hidden lg:block'} />
            <NavbarLinks className="hidden lg:block" />
          </div>
        </div>
      </div>
    </Affix>
  );
}
