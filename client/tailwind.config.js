/** @type {import('tailwindcss').Config} */
import { colors as defaultColors, fontFamily as defaultFontFamily } from 'tailwindcss/defaultTheme';

export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        ...defaultColors,
        ...{
          'theme-green': '#48d597'
        }
      }
    },
    fontFamily: {
      'sans': ["'PP Object Sans'", ...defaultFontFamily.sans]
    }
  },
  plugins: [],
  corePlugins: {
    preflight: false
  }
};
