function FooterAppAdvert() {
  return (
    <div className="col-span-1 flex flex-col lg:col-span-2">
      <div className="flex items-center justify-center lg:justify-start">
        <img
          width="90"
          height="90"
          alt="mobile app qr code"
          className="mr-6 hidden lg:block"
          src="https://shafa.c.prom.st/build/src/components/AppsPromotionQRCode/images/footer.png"
        />
        <div className="flex flex-col">
          <p className="my-0 text-base font-medium">Завантажуйте застосунок</p>
          <p className="mt-2 text-sm">Купуйте речі і спілкуйтесь у будь-якому місці</p>
        </div>
      </div>
      <div className="mt-4">
        <a href="https://apple.co/2pE66cJ" className="mr-2 " target="_blank" rel="noopener noreferrer">
          <img
            width="152"
            height="48"
            alt="App Store"
            className="rounded-lg"
            src="https://shafa.c.prom.st/build/src/components/Footer/images/app-store-2x-min.jpg"
          />
        </a>
        <a
          href="https://play.google.com/store/apps/details?id=shafa.odejda.obuv.aksessuary"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img
            width="152"
            height="48"
            className="rounded-lg"
            alt="Google Play"
            src="https://shafa.c.prom.st/build/src/components/Footer/images/google-play-2x-min.jpg"
          />
        </a>
      </div>
    </div>
  );
}

export default FooterAppAdvert;
