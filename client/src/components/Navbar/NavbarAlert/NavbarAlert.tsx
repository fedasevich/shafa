import { Button } from 'antd';

function NavbarAlert() {
  return (
    <div className="flex justify-start bg-theme-green p-3 font-sans xl:justify-center xl:p-0">
      <div className="flex w-3/4 flex-col justify-evenly xl:flex-row">
        <div className="flex flex-col text-sm xl:flex-row">
          <p className="m-0 mr-3 self-start font-medium xl:self-center">Речі за кліком серця</p>
          <p className="m-0 self-start xl:self-center">23878456 позицій в продажі</p>
          <img className="hidden xl:block" src="https://shafa.c.prom.st/img/index-dress.png" alt="test" />
        </div>
        <Button className="border-1 mt-2 h-6 self-start border-black bg-transparent py-0 text-center text-xs xl:mt-0 xl:self-center">
          Зарeєструватися
        </Button>
      </div>
    </div>
  );
}

export default NavbarAlert;
