import { UserCurrency } from './UserCurrency.type';

export type User = {
  isAuth: boolean;
  userCurrency: UserCurrency;
};
