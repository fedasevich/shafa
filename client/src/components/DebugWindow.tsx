import { useContext } from 'react';
import { HistoryContext } from './AppRouter';

export function DebugWindow() {
  const visitedRoutes = useContext(HistoryContext);

  if (!visitedRoutes) {
    return null;
  }

  return (
    <div className="fixed bottom-5 right-5 z-50 w-1/4 rounded-lg bg-theme-green p-5">
      {visitedRoutes.map((item, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <p key={index}>
          Visited &quot;{item.path}&quot; at {item.visitedAt}
        </p>
      ))}
    </div>
  );
}
