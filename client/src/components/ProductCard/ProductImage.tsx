import { PRODUCT_ROUTE_$ID } from '#/libs/constants/routes';
import { cn } from '#/libs/helpers/cn';
import useHistoricNavigate from '#/libs/hooks/useHistoricNavigate';
import { ClassNamed } from '#/libs/types/ClassNamed/ClassNamed.type';
import { ReactNode, useState } from 'react';
import { Link, generatePath } from 'react-router-dom';
import { useProductCardContext } from './ProductCardContext';

interface ProductImageProps extends ClassNamed {
  top?: ReactNode;
  bottom?: ReactNode;
}

function ProductImage({ bottom, top, className }: ProductImageProps) {
  const [isLoading, setIsLoading] = useState(true);

  const handleImageLoad = () => {
    setIsLoading(false);
  };

  const {
    product: { thumbnail, name, id }
  } = useProductCardContext();

  const { onLinkClick } = useHistoricNavigate();

  const generatedPath = generatePath(PRODUCT_ROUTE_$ID, { id: id.toString() });

  const handleLinkClick = () => {
    onLinkClick(generatedPath);
  };

  return (
    <Link to={generatedPath} onClick={handleLinkClick} className={cn('relative block w-full', className)}>
      {top}
      {isLoading && (
        <div className="m-auto flex h-[240px] w-60 items-center justify-center">
          <img src="/images/catalog-hanger.png" alt="Placeholder" className="h-20 w-20" />
        </div>
      )}
      <img src={thumbnail} onLoad={handleImageLoad} alt={name} className="w-full rounded-[32px]" loading="lazy" />
      {bottom}
    </Link>
  );
}

export default ProductImage;
