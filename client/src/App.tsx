import { ConfigProvider } from 'antd';
import { BrowserRouter } from 'react-router-dom';
import AppRouter from './components/AppRouter';
import Footer from './components/Footer/Footer';
import { Header } from './components/Header/Header';
import { getCustomFontFamily } from './libs/helpers/getCustomFontFamily';
import ScrollToTop from './libs/helpers/scrollToTop';

export const CUSTOM_FONT = "'PP Object Sans'";

export default function App() {
  return (
    <BrowserRouter>
      <ConfigProvider
        theme={{
          token: {
            fontFamily: getCustomFontFamily()
          }
        }}
      >
        <ScrollToTop />
        <Header isShowAlert />
        <AppRouter />
        <Footer />
      </ConfigProvider>
    </BrowserRouter>
  );
}
