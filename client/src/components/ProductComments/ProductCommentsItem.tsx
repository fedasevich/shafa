import { Comment } from '#/libs/types/Products/Comment.type';
import { ProductRating } from '../ProductRating/ProductRating';

interface ProductCommentsItemProps {
  comment: Comment;
}

export function ProductCommentsItem({ comment }: ProductCommentsItemProps) {
  return (
    <div className="flex justify-between border-x-0 border-b-2 border-t-0 border-solid border-gray-200">
      <div className="">
        <p className="my-0 text-sm font-medium">{comment.from}</p>
        <ProductRating filled={comment.rating} />
        <p className="mt-1 text-sm">{comment.text}</p>
      </div>
      <p className="mt-1 text-sm text-gray-400">{comment.date}</p>
    </div>
  );
}
