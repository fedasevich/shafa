import { useProductCardContext } from './ProductCardContext';

function ProductBrand() {
  const {
    product: { brand }
  } = useProductCardContext();
  return (
    <div className="absolute bottom-1 z-[2] flex h-24 w-full items-end rounded-[32px] bg-opacity-40 bg-gradient-to-b from-transparent via-transparent to-[#00000080]">
      <span className="mb-2 ml-4 w-full text-lg font-medium text-white">{brand?.name}</span>
    </div>
  );
}

export default ProductBrand;
