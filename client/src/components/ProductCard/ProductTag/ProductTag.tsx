import { Tag } from '../../../libs/types/Products/Tag.type';
import { useProductCardContext } from '../ProductCardContext';
import FreeDeliveryTag from './FreeDeliveryTag';
import UkrainianTag from './UkrainianTag';

interface ProductTagProps {
  icon: React.ReactNode;
  tag: Tag;
}

function ProductTag({ icon, tag }: ProductTagProps) {
  const {
    product: { tags }
  } = useProductCardContext();

  if (!tags.includes(tag)) {
    return null;
  }

  return (
    <div className="rounded-18 mr-1 flex h-2 w-5 items-center justify-center rounded-2xl bg-white p-2">{icon}</div>
  );
}

ProductTag.FreeDelivery = FreeDeliveryTag;
ProductTag.Ukrainian = UkrainianTag;

export default ProductTag;
