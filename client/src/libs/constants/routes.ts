export const SHOP_ROUTE = '/';
export const SHOP_ROUTE_CATEGORY = '/category/:category';
export const CART_ROUTE = '/cart';
export const PRODUCT_ROUTE_$ID = '/product/:id';
export const PRIVACY_POLICY_ROUTE = '/privacy-policy';
export const TERMS_OF_SERVICE_ROUTE = '/terms-of-service';
export const ADMIN_ROUTE = '/admin';
